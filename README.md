# SISTEMA WEB - Configuración

## Estructura
* Base de datos: config/db.py (modificar password)
* Los modelos están en: core/nombremodelo/models.py
* Las rutas de los modelos están en core/nombremodelo/urls.py
* En config/urls.py se incluyen todas las rutas
* En core/nombremodelo/admin.py están registrados los modelos
* En core/nombremodelo/views.py y en core/nombremodelo/views están los controladores
* Las vistas están en templates/nombremodelo

## Notas
* Se realizó el "Restore" de la base de datos
* Se creó el entorno virtual con el comando "python -m venv env"

## Entorno Virtual
* Crear una carpeta
* En la carpeta, ejecutar el comando "python -m venv env"
* Ingresar a la carpeta "env/Scripts" y activar el entorno virtual con "activate"
* Con el entorno activado, instalar freeze (comando pip install freeze), instalar django (comando pip install django)
* Volver a la carpeta del proyecto (cd .., cd ..) y ejecutar e comando "django-admin startproject app"
* ACLARACION: Debe quedar una carpeta "app" y otra "env" 

## Github
* Abrir repositorio
* En "code" seleccionar la rama (por defecto está en la rama main o master). Cuando se despliega el menú, muestra la opción de "buscar o crear una rama"

### Proyecto
* En el caso que se haga una nueva funcionalidad, crear una rama en VSC (tener en cuenta que la rama se crear desde la rama actual). En la parte de abajo, a la izquierda, aparece la rama actual, hacer click y se despliega un menú con la opción de crear rama, o cambiar de rama.
* En la nueva rama, crear y modificar los archivos que se requieran, y en la parte del costado izquierdo, aparece la opción de "Control de código". Hacer click, y con el botón "+", agregar los archivos.
* Luego hacer commit y push de los archivos.
* ACLARACION: Cuando se termina el trabajo en la rama, ir al repositorio de github, y hacer un "pull request" (ver que aparezca desde la rama base, a la rama actual). 
 
